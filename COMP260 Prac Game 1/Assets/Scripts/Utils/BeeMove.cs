﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	public GameObject P1;
	public GameObject P2;

	// public parameters with default values
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	// private state
	public float speed;        // metres per second
	public float turnSpeed;  // degrees per second

	public Transform target;
	public Vector2 heading;

    // Use this for initialization
    void Start() {
		P1 = GameObject.Find("Player1");
        if(P2 == null)
		    P2 = GameObject.Find("Player2");

		// find the player to set the target
		PlayerMove player = FindObjectOfType<PlayerMove> ();
		target = player.transform;

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		// set speed and turnSpeed randomly
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);

    }


    void Update()
    {
        checkGameOver();
		findTarget ();
        moveTowardsTarget();
    }

    void checkGameOver()
    {

    }

    void moveTowardsTarget()
    {
        // get the vector from the bee to the target 
        Vector2 direction = target.position - transform.position;

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);

    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

	void findTarget() {
        if (P2 == null)
            target = P1.transform;
        else if ((Vector2.Distance(P1.transform.position, transform.position) < Vector2.Distance(P2.transform.position, transform.position)))
        {
            target = P1.transform;
        }
        else if ((Vector2.Distance(P1.transform.position, transform.position) > Vector2.Distance(P2.transform.position, transform.position)))
        {
            target = P2.transform;
        }
        // TODO: Currently only works w/two players. Make game work w/one player. 
        else
            target = P1.transform;
	}

	public ParticleSystem explosionPrefab;

	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate (explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}
}

