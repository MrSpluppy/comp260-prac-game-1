﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {

	public int nBees = 50;
    private int curBees = 0;
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;

    public float minBeePeriod, maxBeePeriod;
    private float beeSpawnRateCount;

    public float countDownTimer = 25f;

    // Use this for initialization
    void Start () {
        beeSpawnRateCount = 0;

        /*
		// create bees
		for (int i = 0; i < nBees; i++) {
			// instantiate a bee
			BeeMove bee = Instantiate (beePrefab);
			// attach to this object in the hierachy
			bee.transform.parent = transform;
			// give the bee a name and number
			bee.gameObject.name = "Bee " + i;

			// move the bee to a random position within the bounding rectangle
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
            
		}
        */
    }

    // Update is called once per frame
    void Update () {
        if (maxBeePeriod > minBeePeriod)
            maxBeePeriod -= Time.deltaTime / countDownTimer;
        else
            maxBeePeriod = minBeePeriod;
        // Counts down in real time till beeSpawnRateCount = 0
        beeSpawnRateCount -= Time.deltaTime;
        print(beeSpawnRateCount);
        // create bees
        if(beeSpawnRateCount < 0)
        {
            if (curBees < nBees)
            {
                // instantiate a bee
                BeeMove bee = Instantiate(beePrefab);
                // attach to this object in the hierachy
                bee.transform.parent = transform;
                // give the bee a name and number
                bee.gameObject.name = "Bee " + curBees;

                // move the bee to a random position within the bounding rectangle
                float x = xMin + Random.value * width;
                float y = yMin + Random.value * height;
                bee.transform.position = new Vector2(x, y);
                // Resets the spawner countdown to the designated time
                beeSpawnRateCount = Random.Range(minBeePeriod, maxBeePeriod);
            }
        }
    }

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within 'radius' of 'centre'
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			// BUG! the line below doesn't work
			Vector2 v = (Vector2)child.position - centre;
			if(v.magnitude <= radius) {
				Destroy (child.gameObject);
			}
		}
	}
}
