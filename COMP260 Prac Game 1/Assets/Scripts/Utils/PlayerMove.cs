﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	//public gameObject P1;
	//public gameObject P2;
	private BeeSpawner beeSpawner;

	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}

    public Vector2 Velocity; // in metres per second
    public float maxSpeed = 5.0f;

	public float destroyRadius = 1.0f;
    // Update is called once per frame

    void Update() {
		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees (
				transform.position, destroyRadius);
		}

		if (gameObject.name == "Player1") {
			// get the input values
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");

			// scale by the maxSpeed parameter
			Vector2 velocity = direction * maxSpeed;

			// move the object
			transform.Translate (velocity * Time.deltaTime);

		} else if (gameObject.name == "Player2") {
			// get the input values
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal2");
			direction.y = Input.GetAxis ("Vertical2");

			// scale by the maxSpeed parameter
			Vector2 velocity = direction * maxSpeed;

			// move the object
			transform.Translate (velocity * Time.deltaTime);
		}
    }
}
