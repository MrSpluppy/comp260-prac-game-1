﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerSelect : MonoBehaviour {

    public GameObject playerTwo;
    public GameObject gameScene;
    public GameObject menuScene;
    public GameObject playerOneText;

	// Use this for initialization
	void Start () {
        gameScene.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void onePlayer() {
		playerOneText.SetActive(false);
        gameScene.SetActive(true);
        playerTwo.SetActive(false);
        menuScene.SetActive(false);
	}

    public void twoPlayer() {
        gameScene.SetActive(true);
        menuScene.SetActive(false);
    }
}
