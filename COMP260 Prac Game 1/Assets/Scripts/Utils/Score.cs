﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score : MonoBehaviour {

    GameObject test;
    Text pointsText;
    float points = 0f;
	// Use this for initialization
	void Start () {
        pointsText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        points += Time.deltaTime;
        pointsText.text = "Score: " + points;
	}
}
